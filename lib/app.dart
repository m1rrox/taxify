import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:taxify/feature/auth/cubit/auth_cubit.dart';
import 'package:taxify/feature/init/bloc/init_bloc.dart';
import 'package:taxify/feature/init/splash_page.dart';
import 'package:taxify/feature/profile/data/repository/profile_repository.dart';
import 'package:taxify/feature/profile/domain/repository/profile_repository.dart';
import 'package:taxify/home_page.dart';

class TaxifyApp extends StatefulWidget {
  const TaxifyApp({Key? key}) : super(key: key);

  @override
  State<TaxifyApp> createState() => _TaxifyAppState();
}

class _TaxifyAppState extends State<TaxifyApp> {
  @override
  void initState() {
    super.initState();
    context.read<InitBloc>().add(const AppStarted());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocBuilder<InitBloc, InitState>(
        builder: (context, state) {
          if (state.isLoading) {
            return const SplashPage();
          }

          return MultiProvider(
            providers: [
              Provider<FirebaseAuth>.value(
                value: FirebaseAuth.instance,
              ),
              Provider<FirebaseFirestore>.value(
                value: FirebaseFirestore.instance,
              ),
              Provider<FirebaseStorage>.value(
                value: FirebaseStorage.instance,
              ),
              Provider<IProfileRepository>(
                create: (context) => ProfileRepository(
                  firestore: context.read<FirebaseFirestore>(),
                ),
              ),
              BlocProvider<AuthCubit>(
                create: (context) => AuthCubit(
                  firebaseAuth: context.read<FirebaseAuth>(),
                ),
              ),
            ],
            child: const HomePage(),
          );
        },
      ),
    );
  }
}

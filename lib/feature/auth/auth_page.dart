import 'package:flutter/material.dart';
import 'package:flutterfire_ui/auth.dart';

class AuthPage extends StatelessWidget {
  const AuthPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SignInScreen(
      providerConfigs: [
        EmailProviderConfiguration(),
        GoogleProviderConfiguration(
          clientId:
              '730610858778-ovo81um8p4tep3h2n8ovb3r6ombjdfuf.apps.googleusercontent.com',
        ),
      ],
    );
  }
}

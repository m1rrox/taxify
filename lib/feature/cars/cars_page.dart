import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taxify/common/page/widgets/loader.dart';
import 'package:taxify/feature/cars/car_rent_page.dart';
import 'package:taxify/feature/cars/domain/bloc/cars_bloc.dart';
import 'package:taxify/feature/cars/widgets/car_card.dart';

class CarsPage extends StatefulWidget {
  const CarsPage({Key? key}) : super(key: key);

  @override
  State<CarsPage> createState() => _CarsPageState();
}

class _CarsPageState extends State<CarsPage> {
  @override
  void initState() {
    super.initState();
    context.read<CarsBloc>().add(const LoadCars());
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<CarsBloc>();

    return Scaffold(
      appBar: AppBar(
        title: const Text('Available cars'),
        centerTitle: true,
      ),
      body: BlocBuilder<CarsBloc, CarsState>(
        builder: (context, state) {
          if (state.isLoading && state.cars.isEmpty) return const Loader();

          return SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              children: state.cars
                  .map(
                    (car) => CarCard(
                      car: car,
                      onPressed: (car) => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => BlocProvider<CarsBloc>.value(
                            value: bloc,
                            child: CarRentPage(car: car),
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
          );
        },
      ),
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:taxify/feature/cars/domain/entity/car.dart';

class CarModel {
  const CarModel({
    required this.id,
    required this.title,
    required this.isFree,
    required this.dailyPrice,
    required this.imageUrl,
    required this.description,
    required this.releaseDate,
  });

  final String id;
  final String title;
  final bool isFree;
  final String imageUrl;
  final double dailyPrice;
  final String description;
  final DateTime? releaseDate;

  factory CarModel.fromDocument(
    DocumentSnapshot<Map<String, dynamic>> document,
    SnapshotOptions? options,
  ) {
    final data = document.data()!;
    return CarModel(
      id: document.id,
      title: data['title'] as String,
      isFree: data['isFree'] as bool,
      imageUrl: data['imageUrl'] as String,
      dailyPrice: data['dailyPrice'] as double,
      description: data['description'] as String,
      releaseDate: (data['releaseDate'] as Timestamp?)?.toDate(),
    );
  }

  factory CarModel.fromEntity(Car entity) => CarModel(
        id: entity.id,
        title: entity.title,
        isFree: entity.isFree,
        imageUrl: entity.imageUrl,
        dailyPrice: entity.dailyPrice,
        description: entity.description,
        releaseDate: entity.releaseDate,
      );

  Map<String, dynamic> toDocument() => {
        'title': title,
        'isFree': isFree,
        'imageUrl': imageUrl,
        'dailyPrice': dailyPrice,
        'description': description,
        'releaseDate': releaseDate,
      };

  Car toEntity() => Car(
        id: id,
        title: title,
        isFree: isFree,
        dailyPrice: dailyPrice,
        imageUrl: imageUrl,
        description: description,
        releaseDate: releaseDate,
      );
}

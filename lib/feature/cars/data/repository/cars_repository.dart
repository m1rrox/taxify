import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:taxify/feature/cars/data/model/car_model.dart';
import 'package:taxify/feature/cars/domain/entity/car.dart';
import 'package:taxify/feature/cars/domain/repository/cars_repository.dart';

class CarsRepository extends ICarsRepository {
  CarsRepository({required this.firestore, required this.currentUserId});

  final String currentUserId;
  final FirebaseFirestore firestore;

  late CollectionReference<CarModel> cars =
      firestore.collection('cars').withConverter(
            fromFirestore: (document, options) =>
                CarModel.fromDocument(document, options),
            toFirestore: (model, options) => model.toDocument(),
          );

  late CollectionReference<CarModel> rentedCars = firestore
      .collection('users')
      .doc(currentUserId)
      .collection('rentedCars')
      .withConverter(
        fromFirestore: (document, options) =>
            CarModel.fromDocument(document, options),
        toFirestore: (model, options) => model.toDocument(),
      );

  @override
  Future<List<Car>> getCars() async {
    final querySnapshot = await cars.get();

    return querySnapshot.docs
        .map((document) => document.data().toEntity())
        .where((car) => car.isFree)
        .toList();
  }

  @override
  Future<List<Car>> getRentedCars() async {
    final querySnapshot = await rentedCars.get();

    return querySnapshot.docs
        .map((document) => document.data().toEntity())
        .toList();
  }

  @override
  Future<Car> rentCar(Car car) async {
    await cars.doc(car.id).set(CarModel.fromEntity(car));

    await rentedCars.doc(car.id).set(CarModel.fromEntity(car));
    return car;
  }
}

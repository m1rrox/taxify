part of 'cars_bloc.dart';

class CarsState extends Equatable {
  const CarsState({
    required this.cars,
    required this.isLoading,
  });

  factory CarsState.empty() => const CarsState(cars: [], isLoading: false);

  CarsState copyWith({List<Car>? cars, bool? isLoading}) => CarsState(
        cars: cars ?? this.cars,
        isLoading: isLoading ?? this.isLoading,
      );

  final List<Car> cars;
  final bool isLoading;

  @override
  List<Object> get props => [cars, isLoading];
}

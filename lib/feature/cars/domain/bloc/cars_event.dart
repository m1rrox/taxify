part of 'cars_bloc.dart';

abstract class CarsEvent extends Equatable {
  const CarsEvent();

  @override
  List<Object> get props => [];
}

class LoadCars extends CarsEvent {
  const LoadCars();

  @override
  List<Object> get props => [];
}

class RentCar extends CarsEvent {
  const RentCar({required this.car});

  final Car car;

  @override
  List<Object> get props => [car];
}

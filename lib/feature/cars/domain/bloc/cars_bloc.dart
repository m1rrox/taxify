import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:taxify/feature/cars/domain/entity/car.dart';
import 'package:taxify/feature/cars/domain/repository/cars_repository.dart';

part 'cars_event.dart';
part 'cars_state.dart';

class CarsBloc extends Bloc<CarsEvent, CarsState> {
  CarsBloc({required this.carsRepository}) : super(CarsState.empty()) {
    on<LoadCars>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      final cars = await carsRepository.getCars();

      emit(state.copyWith(cars: cars, isLoading: false));
    });

    on<RentCar>((event, emit) async {
      final cars = await carsRepository.rentCar(event.car);
      add(const LoadCars());
    });
  }

  final ICarsRepository carsRepository;
}

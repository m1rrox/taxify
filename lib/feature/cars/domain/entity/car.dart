import 'package:equatable/equatable.dart';

enum CarType { suv, sedan, coupe }

class Car extends Equatable {
  const Car({
    required this.id,
    required this.title,
    required this.isFree,
    required this.dailyPrice,
    required this.imageUrl,
    required this.description,
    required this.releaseDate,
  });

  Car copyWith({DateTime? releaseDate, bool? isFree}) => Car(
        id: id,
        title: title,
        isFree: isFree ?? this.isFree,
        dailyPrice: dailyPrice,
        imageUrl: imageUrl,
        description: description,
        releaseDate: releaseDate ?? this.releaseDate,
      );

  final String id;
  final String title;
  final bool isFree;
  final String imageUrl;
  final double dailyPrice;
  final String description;
  final DateTime? releaseDate;

  @override
  List<Object?> get props => [
        id,
        title,
        description,
        imageUrl,
        dailyPrice,
        isFree,
        description,
        releaseDate,
      ];
}

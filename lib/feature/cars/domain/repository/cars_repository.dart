import 'package:taxify/feature/cars/domain/entity/car.dart';

abstract class ICarsRepository {
  Future<List<Car>> getCars();

  Future<List<Car>> getRentedCars();

  Future<Car> rentCar(Car car);
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/subjects.dart';
import 'package:taxify/feature/cars/domain/bloc/cars_bloc.dart';
import 'package:taxify/feature/cars/domain/entity/car.dart';

class CarRentPage extends StatefulWidget {
  const CarRentPage({Key? key, required this.car}) : super(key: key);

  final Car car;

  @override
  State<CarRentPage> createState() => _CarRentPageState();
}

class _CarRentPageState extends State<CarRentPage> {
  final DateFormat dateFormater = DateFormat.MMMd();
  late BehaviorSubject<DateTime> _releaseAt;

  @override
  void initState() {
    super.initState();
    _releaseAt = BehaviorSubject.seeded(
      DateTime.now().add(const Duration(days: 1)),
    );
  }

  @override
  void dispose() {
    _releaseAt.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.car.title),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              height: 250.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(widget.car.imageUrl),
                ),
                color: Colors.blue.shade100,
                borderRadius: BorderRadius.circular(20.0),
              ),
            ),
            const SizedBox(height: 16.0),
            Text(
              widget.car.description,
              maxLines: 4,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 16.0),
            StreamBuilder<DateTime>(
              stream: _releaseAt,
              builder: (context, snapshot) {
                return OutlinedButton(
                  onPressed: () => _showDatePicker(
                    context,
                    onChanged: (dateTime) => _releaseAt.add(dateTime),
                    initialDate: snapshot.data!,
                  ),
                  child: Text(
                    'Release date: ${dateFormater.format(snapshot.data!)}',
                  ),
                );
              },
            ),
            StreamBuilder<DateTime>(
              stream: _releaseAt,
              builder: (context, snapshot) {
                final price =
                    snapshot.data!.difference(DateTime.now()).inHours /
                        24.0 *
                        widget.car.dailyPrice;

                return ElevatedButton(
                  onPressed: () {
                    context.read<CarsBloc>().add(
                          RentCar(
                            car: widget.car.copyWith(
                              releaseDate: _releaseAt.value,
                              isFree: false,
                            ),
                          ),
                        );
                    Navigator.of(context).pop();
                  },
                  child: Text('Rent for \$${price.toStringAsFixed(2)}'),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  void _showDatePicker(
    BuildContext context, {
    required Function(DateTime) onChanged,
    required DateTime initialDate,
  }) {
    showCupertinoModalPopup(
      context: context,
      builder: (_) => Container(
        height: 400,
        color: Colors.white,
        child: CupertinoDatePicker(
          mode: CupertinoDatePickerMode.dateAndTime,
          minimumDate: DateTime.now(),
          maximumDate: DateTime.now().add(const Duration(days: 30)),
          initialDateTime: initialDate,
          onDateTimeChanged: onChanged,
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:taxify/feature/cars/domain/entity/car.dart';

class CarCard extends StatelessWidget {
  const CarCard({Key? key, required this.car, required this.onPressed})
      : super(key: key);

  final Car car;
  final Function(Car) onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      child: Card(
        elevation: 1.5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        color: Colors.blue.shade50,
        shadowColor: Colors.black26,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                width: 110.0,
                decoration: BoxDecoration(
                  color: Colors.blue.shade400,
                  borderRadius: const BorderRadius.horizontal(
                    left: Radius.circular(12.0),
                  ),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(car.imageUrl),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(
                      car.title,
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      '\$${car.dailyPrice}/d',
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      car.description,
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              if (car.isFree)
                InkWell(
                  splashColor: Colors.white30,
                  onTap: () => onPressed(car),
                  child: Ink(
                    width: 60.0,
                    decoration: const BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.horizontal(
                        right: Radius.circular(12.0),
                      ),
                    ),
                    child: const Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}

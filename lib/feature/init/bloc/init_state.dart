part of 'init_bloc.dart';

class InitState extends Equatable {
  const InitState({required this.isLoading});

  factory InitState.empty() => const InitState(isLoading: true);

  final bool isLoading;

  InitState copyWith({bool? isLoading}) => InitState(
        isLoading: isLoading ?? this.isLoading,
      );

  @override
  List<Object> get props => [isLoading];
}

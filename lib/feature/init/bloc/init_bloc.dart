import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_core/firebase_core.dart';

import '../../../firebase_options.dart';

part 'init_event.dart';
part 'init_state.dart';

class InitBloc extends Bloc<InitEvent, InitState> {
  InitBloc() : super(InitState.empty()) {
    on<AppStarted>((event, emit) => _appStarted(event, emit));
  }

  void _appStarted(AppStarted event, Emitter<InitState> emit) async {
    emit(state.copyWith(isLoading: true));

    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );

    await Future.delayed(
      const Duration(seconds: 2),
    );

    emit(state.copyWith(isLoading: false));
  }
}

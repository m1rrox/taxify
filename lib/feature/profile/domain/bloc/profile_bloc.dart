import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:taxify/feature/cars/domain/entity/car.dart';
import 'package:taxify/feature/cars/domain/repository/cars_repository.dart';
import 'package:taxify/feature/profile/domain/entity/profile.dart';
import 'package:taxify/feature/profile/domain/repository/profile_repository.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc({
    required this.profileRepository,
    required this.carsRepository,
  }) : super(ProfileState.empty()) {
    on<UserLoggedIn>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      await profileRepository.createProfile(event.profile);
      final profile = await profileRepository.getProfileById(event.profile.uid);

      if (profile != null) emit(state.copyWith(profile: profile));

      emit(state.copyWith(isLoading: false));
    });

    on<LoadRentedCars>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      final rentedCars = await carsRepository.getRentedCars();

      emit(state.copyWith(rentedCars: rentedCars, isLoading: false));
    });
  }

  final IProfileRepository profileRepository;
  final ICarsRepository carsRepository;
}

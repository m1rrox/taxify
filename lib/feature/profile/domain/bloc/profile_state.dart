part of 'profile_bloc.dart';

class ProfileState extends Equatable {
  const ProfileState({
    required this.isLoading,
    required this.profile,
    required this.rentedCars,
  });

  factory ProfileState.empty() => ProfileState(
        isLoading: false,
        profile: Profile.empty(),
        rentedCars: const [],
      );

  final bool isLoading;
  final Profile profile;
  final List<Car> rentedCars;

  ProfileState copyWith({
    bool? isLoading,
    Profile? profile,
    List<Car>? rentedCars,
  }) =>
      ProfileState(
        isLoading: isLoading ?? this.isLoading,
        profile: profile ?? this.profile,
        rentedCars: rentedCars ?? this.rentedCars,
      );

  @override
  List<Object> get props => [isLoading, profile, rentedCars];
}

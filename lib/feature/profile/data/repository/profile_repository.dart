import '../model/profile_model.dart';
import '../../domain/entity/profile.dart';
import '../../domain/repository/profile_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ProfileRepository extends IProfileRepository {
  ProfileRepository({required this.firestore});

  final FirebaseFirestore firestore;
  late CollectionReference<ProfileModel> users =
      firestore.collection('users').withConverter(
            fromFirestore: (document, options) =>
                ProfileModel.fromDocument(document, options),
            toFirestore: (model, options) => model.toDocument(),
          );

  @override
  Future<Profile> createProfile(Profile profile) async {
    await users.doc(profile.uid).set(ProfileModel.fromEntity(profile));

    return profile;
  }

  @override
  Future<Profile?> getProfileById(String id) =>
      users.doc(id).get().then((value) => value.data()?.toEntity());
}

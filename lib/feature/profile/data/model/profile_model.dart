import 'package:cloud_firestore/cloud_firestore.dart';
import '../../domain/entity/profile.dart';

class ProfileModel {
  final String id;
  final String? name;
  final String? avatarUrl;

  ProfileModel({
    required this.id,
    this.name,
    this.avatarUrl,
  });

  factory ProfileModel.fromDocument(
    DocumentSnapshot<Map<String, dynamic>> document,
    SnapshotOptions? options,
  ) {
    final data = document.data()!;
    return ProfileModel(
      id: document.id,
      name: data['name'] as String?,
      avatarUrl: data['avatarUrl'] as String?,
    );
  }

  factory ProfileModel.fromEntity(Profile entity) => ProfileModel(
        id: entity.uid,
        name: entity.name,
        avatarUrl: entity.avatarUrl,
      );

  Map<String, dynamic> toDocument() => {
        'name': name,
        'avatarUrl': avatarUrl,
      };

  Profile toEntity() => Profile(
        uid: id,
        name: name,
        avatarUrl: avatarUrl,
      );
}

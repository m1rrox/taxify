import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taxify/app.dart';
import 'package:taxify/feature/init/bloc/init_bloc.dart';

void main() {
  runApp(BlocProvider<InitBloc>(
    create: (context) => InitBloc(),
    child: const TaxifyApp(),
  ));
}

import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taxify/feature/cars/cars_page.dart';
import 'package:taxify/feature/profile/domain/bloc/profile_bloc.dart';
import 'package:taxify/feature/profile/domain/entity/profile.dart';
import 'package:taxify/feature/profile/profile_page.dart';

class AppRootPage extends StatefulWidget {
  const AppRootPage({Key? key}) : super(key: key);

  @override
  State<AppRootPage> createState() => _AppRootPageState();
}

class _AppRootPageState extends State<AppRootPage> {
  final int initialPageIndex = 0;

  late PageController _pageController;
  late BehaviorSubject<int> _selectedPage;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: initialPageIndex);
    _selectedPage = BehaviorSubject<int>.seeded(initialPageIndex);
  }

  @override
  void dispose() {
    _selectedPage.close();
    _pageController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    context.read<ProfileBloc>()
      ..add(
        UserLoggedIn(profile: context.read<Profile>()),
      )
      ..add(const LoadRentedCars());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        physics: const BouncingScrollPhysics(),
        onPageChanged: (selectedPage) => _selectedPage.add(selectedPage),
        children: const <Widget>[
          CarsPage(),
          ProfilePage(),
        ],
      ),
      bottomNavigationBar: ClipRRect(
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(24.0),
        ),
        child: NavigationBarTheme(
          data: NavigationBarThemeData(
            indicatorColor: Colors.blue.shade100,
          ),
          child: StreamBuilder<int>(
            stream: _selectedPage,
            builder: (context, selectedPage) {
              return NavigationBar(
                height: 60.0,
                selectedIndex: selectedPage.data!,
                backgroundColor: Colors.blue.shade50,
                animationDuration: const Duration(milliseconds: 300),
                onDestinationSelected: (selectedDestination) =>
                    _pageController.animateToPage(
                  selectedDestination,
                  duration: const Duration(milliseconds: 500),
                  curve: Curves.easeInOut,
                ),
                destinations: <Widget>[
                  const NavigationDestination(
                    icon: Icon(Icons.car_rental_outlined),
                    label: 'Cars',
                    selectedIcon: Icon(Icons.car_rental_rounded),
                  ),
                  NavigationDestination(
                    icon: context.read<Profile>().avatarUrl != null
                        ? CircleAvatar(
                            radius: 12.0,
                            backgroundImage: NetworkImage(
                              context.read<Profile>().avatarUrl!,
                            ),
                          )
                        : const Icon(Icons.person_outlined),
                    label: 'Profile',
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

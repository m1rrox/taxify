import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taxify/feature/auth/cubit/auth_cubit.dart';
import 'package:taxify/feature/cars/data/repository/cars_repository.dart';
import 'package:taxify/feature/cars/domain/bloc/cars_bloc.dart';
import 'package:taxify/feature/cars/domain/repository/cars_repository.dart';
import 'package:taxify/feature/profile/domain/bloc/profile_bloc.dart';
import 'package:taxify/feature/profile/domain/entity/profile.dart';
import 'package:taxify/feature/profile/domain/repository/profile_repository.dart';

import 'common/page/app_root_page.dart';
import 'feature/auth/auth_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (context, state) {
        if (state is Authenticated) {
          return MultiProvider(
            providers: [
              Provider<Profile>.value(value: state.profile),
              Provider<ICarsRepository>(
                create: (context) => CarsRepository(
                  currentUserId: context.read<Profile>().uid,
                  firestore: context.read<FirebaseFirestore>(),
                ),
              ),
              BlocProvider<ProfileBloc>(
                create: (context) => ProfileBloc(
                  profileRepository: context.read<IProfileRepository>(),
                  carsRepository: context.read<ICarsRepository>(),
                ),
              ),
              BlocProvider<CarsBloc>(
                create: (context) => CarsBloc(
                  carsRepository: context.read<ICarsRepository>(),
                ),
              ),
            ],
            child: const AppRootPage(),
          );
        }

        return const AuthPage();
      },
    );
  }
}
